<?php

namespace Modules\Order\Models;

//use App\Models\Tenant\Catalogs\IdentityDocumentType;
use App\Models\Tenant\ModelTenant;
use App\Models\Tenant\Establishment;
class Carretatracto extends ModelTenant
{

  //  protected $with = ['identity_document_type'];
 
    protected $fillable = [
        'tarjetap',
        'tarjetac',
        'soat',
        'revisiont',
        'bonificacion',
        'alarma',
        'conos',
        'tacos',
        'toldera',
        'plastico',
        'soga',
        'malla',
        'neumatico',
        'palosc',
        'palosa',
        'latas',
        'extintort',
        'extintorc',
        'fajas',
        'ratchet',
        'gancho',
        'portallanta',
        'portapalos',
        'lucesd',
        'lucesi',
        'lucesp',
        'lucesint',
        'lucesre',
        'lucesfre',
        'neblineros',
        'vigia',
        'aire',
        'llanta1',
        'llanta2',
        'llanta3',
        'llanta4',
        'llanta5',
        'llanta6',
        'llanta7',
        'llanta8',
        'llanta9',
        'llanta10',
        'llanta11',
        'llanta12',
        'llanta13',
        'llanta14',
        'observaciones',
        'imagen1',
        'imagen2',
        'imagen3',
        'fecha',
        'filename',
        'desc1',
        'desc2',
        'desc3',
        'external_id',
        'placa',
        'responsable',
        'establishment_id',

        
    ];
    protected $casts = [
      
    
  ];
    public function establishment()
    {
        return $this->belongsTo(Establishment::class);
    }

   // public function identity_document_type()
   // {
       // return $this->belongsTo(IdentityDocumentType::class, 'identity_document_type_id');
   // }

}
