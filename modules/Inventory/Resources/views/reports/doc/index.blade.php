@extends('tenant.layouts.app')

@section('content')
@push('scripts')
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
$('#dataFabricatiei').datepicker({
    defaultDate: new Date()
   
});

$('#dataFabricatiei2').datepicker("setDate", new Date());
</script>

@endpush
    <div class="row">
        <div class="col-md-20">
            <div class="card card-primary">
                <div class="card-header">
                    <div>
                        <h4 class="card-title">Consulta Documentos</h4>
                    </div>
                </div>
                <div class="card-body">
                    <div>
                        <form action="{{route('reports.doc.index')}}" class="el-form demo-form-inline el-form--inline" method="POST">
                            {{csrf_field()}}
                            {{-- <div class="el-form-item col-xs-12">
                                <div class="el-form-item__content">
                                    <button class="btn btn-custom" type="submit"><i class="fa fa-search"></i> Buscar</button>
                                </div>
                            </div> --}}
                        </form>
                    </div>
                   
                    <div class="box">
                        <div class="box-body no-padding">
 
                            <div style="margin-bottom: 10px" class="row">

                                <div style="padding-top: 0.5%" class="col-md-14">
                                
                                    <form action="{{route('reports.doc.index')}}" method="get">
                                        {{csrf_field()}}
                                        <input  id="ruc" name="ruc" type="hidden" value=" {{$company->number }}"/>
                                        <div class="row">
                                           
                                        <div class="col-md-2">
                                           <b> Vehiculo </b>
                                                <select class="form-control" name="warehouse_id" id="" style="width: 200px;">
                                                    <option {{ request()->warehouse_id == 'all' ?  'selected' : ''}} selected value="all">Todos</option>
                                                    @foreach($vehiculos as $item)
                                                    <option {{ request()->warehouse_id == $item->id ?  'selected' : ''}} value="{{$item->placa}} - {{$item->marca}}">{{$item->placa}} - {{$item->marca}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                         
                                            <div class="col-md-2">
                                           <b> Clientes </b>
                                                <select class="form-control" name="idperson" id="" style="width: 200px;">
                                                    <option {{ request()->id == 'all' ?  'selected' : ''}} selected value="all">Todos</option>
                                                    @foreach($person as $item)
                                                    <option {{ request()->id == $item->id ?  'selected' : ''}} value="{{$item->id}}">{{$item->id}} - {{$item->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            
                                            <div class="col-md-2">
                                            
                                            <b> Fecha inicio </b>
                                        <input  id="dataFabricatiei" name="dataFabricatiei" type="date" style="border: 1px solid #ced4da;
    border-radius: 0.25rem;
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;height: 37px;" 
    value="{{request()->dataFabricatiei}}"
     />
                                            </div>
                                            <div class="col-md-2">
                                            
                                            <b> Fecha Final </b>
                                        <input  id="dataFabricatiei2" name="dataFabricatiei2" type="date"  style="border: 1px solid #ced4da;
    border-radius: 0.25rem;
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;height: 37px;"
    value="{{request()->dataFabricatiei2}}"
    />
                                            </div>
                                            <div class="col-md-2"> 
                                            <b>.</b>
                                            <button class="btn btn-primary" style="padding-top: 20px;" type="submit"><i class="fa fa-search"></i> Buscar</button></div>
                                        </div>
                                       
                                        
                                    </form>
                                </div>
                                @if(isset($reports))
                                    <div class="col-md-12">
                                        

                                        <form action="{{route('reports.doc.report_excel')}}" class="d-inline" method="POST">
                                            {{csrf_field()}}
                                            <input  id="ruc" name="ruc" type="hidden" value=" {{$company->number }}"/>
                                            <input type="hidden" name="tipo" value="{{request()->tipo}}">
                                            <input type="hidden" name="idperson" value="{{request()->idperson}}">
                                            <input  id="dataFabricatiei" name="dataFabricatiei" type="date" 
                                            value="{{request()->dataFabricatiei}}"
                                            style="border: 1px solid #ced4da;
    border-radius: 0.25rem;
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;height: 37px;display: none;"  />
                                            <input  id="dataFabricatiei2" name="dataFabricatiei2" type="date" 
                                            value="{{request()->dataFabricatiei2}}"
                                            style="border: 1px solid #ced4da;
    border-radius: 0.25rem;
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;height: 37px;display: none;"  />
                                            <input type="hidden" name="warehouse_id" value="{{request()->warehouse_id ? request()->warehouse_id : 'all'}}">
                                            <button class="btn btn-custom   mt-2 mr-2" type="submit"><i class="fa fa-file-excel"></i> Exportar Excel</button>
                                            {{-- <label class="pull-right">Se encontraron {{$reports->count()}} registros.</label> --}}
                                        </form>
                                    </div>

                                @endif


                            </div>
                            <table width="100%" class="table table-striped table-responsive-xl table-bordered table-hover" >
                                <thead class="">
                                    <tr>
                                        <th>#</th>
                                        <th>Serie</th>
                                        <th>Numero</th>
                                        <th>Fecha Emisión </th>
                                        <th>Cliente</th>
                                        <th>IGV</th>
                                        <th>TOTAL</th>
                                        <th>PENDIENTE</th>
                                        
                                    </tr>
                                </thead>
                                @php 
                                $totalgasto=0;
                                $pendientetotal=0;
                                @endphp
                                <tbody>
                                    @foreach($reports as $key => $value)
                                    @php
                                        
                                        $totalgasto = $totalgasto + $value->total;
                                        $pendientetotal=$pendientetotal+($value->total-$value->pendiente);
                                    @endphp
                                    <tr>
                                        <td class="celda">{{$loop->iteration}}</td>   
                                        <td class="celda">{{$value->series}}</td> 
                                        <td class="celda">{{$value->number}}</td>  
                                        <td class="celda">{{$value->date_of_issue}}</td>                                       
                                        <td class="celda">{{$value->cliente}}</td>                                       
                                        <td class="celda">{{$value->total_taxes}}</td>
                                        <td class="celda">{{$value->total}}</td>  
                                        <td class="celda">{{$value->total-$value->pendiente}}</td>                                        
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <table>
                                <tr>                   
                                <td><b> Total : {{$reports->total()}} </b></td>            
                           
                            </tr>
                            <tr>                   
                                      
                                <td><b> Total Facturado : {{ $totalgasto}}</b></td>
                                  
                            </tr>
                            <tr>                   
                              
                               <td><b> Total Pendiente : {{ $pendientetotal}}</b></td>    
                            </tr>
                                
                            </table>
                            <label class="pagination-wrapper ml-2">
                                {{$reports->appends($_GET)->render()}}
                            </label>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
@endsection


