@php
    $establishment = $document->establishment;
    $supplier = $document->supplier;
    
    $tittle =str_pad($document->id, 8, '0', STR_PAD_LEFT); 
@endphp
<html>
<head>
    {{--<title>{{ $tittle }}</title>--}}
    
    
    </style>
</head>
<body>
<div class="ticket">
<table class="full-width">
    <tr>
       
        <td width="50%" >
            <div class="text-center">
            
                <h4 class=""><b>{{$company->name }}</b></h4>
                <h5><b>{{'RUC '.$company->number }}</b></h5>
                
            </div>
        </td>
       
    </tr>
    <tr>
    <td>
   
            
            <h5 class="text-center"><b>Nro de Pago: {{ $tittle }}</b></h5>
            
     </td>
    </tr>
    
</table>
<table class="full-width mt-5">
   
<tr>
        <td width="40%">Numero Gasto:</td>
        <td width="50%">  {{ $document->expense_id }}</td>
        
    </tr>
    <tr>
        <td width="40%">N° Factura/boleta	</td>
        <td width="50%">  {{ $document->reference }}</td>
        
    </tr>
    <tr>
        <td width="40%">Monto:</td>
        <td width="50%">  {{ $document->payment }}</td>
        
    </tr>
    <tr>
        <td width="40%">Método de gasto	:</td>
        <td width="50%"> CAJA GENERAL	 </td>
        
    </tr>
    <tr>
         <td width="30%">Fecha:</td>
        <td width="60%">{{ $document->date_of_payment->format('Y-m-d') }}</td>
    </tr>
  
  
   
    
   
</table>
 


<table class="full-width" style="margin-top:40px">
    <tr>
         
            <td colspan="6" class="border-bottom"></td>
        
    </tr>
    <tr>
         
            <td colspan="6" style="margin-left:20px"><b> FIRMA </b></td>
        
    </tr>
</table>
</div>
</body>
</html>
